# Sourcing example
``` terraform
module "metallb" {
  source                                 = "git::https://gitlab.com/Stargeras/module-metallb"
  kubernetes_auth_host                   = module.kubernetes.kubernetes_auth_host
  kubernetes_auth_cluster_ca_certificate = module.kubernetes.kubernetes_auth_cluster_ca_certificate
  kubernetes_auth_client_certificate     = module.kubernetes.kubernetes_auth_client_certificate
  kubernetes_auth_client_key             = module.kubernetes.kubernetes_auth_client_key
  kubernetes_auth_token                  = module.kubernetes.kubernetes_auth_token
  metallb_addresses                      = "23.82.1.70/32"
}
```
<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_metallb"></a> [metallb](#module\_metallb) | ./modules/metallb | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_kubernetes_auth_client_certificate"></a> [kubernetes\_auth\_client\_certificate](#input\_kubernetes\_auth\_client\_certificate) | PEM-encoded client certificate for TLS authentication | `string` | `""` | no |
| <a name="input_kubernetes_auth_client_key"></a> [kubernetes\_auth\_client\_key](#input\_kubernetes\_auth\_client\_key) | PEM-encoded client certificate key for TLS authentication | `string` | `""` | no |
| <a name="input_kubernetes_auth_cluster_ca_certificate"></a> [kubernetes\_auth\_cluster\_ca\_certificate](#input\_kubernetes\_auth\_cluster\_ca\_certificate) | PEM-encoded root certificates bundle for TLS authentication | `string` | n/a | yes |
| <a name="input_kubernetes_auth_host"></a> [kubernetes\_auth\_host](#input\_kubernetes\_auth\_host) | The hostname (in form of URI) of the Kubernetes API | `string` | n/a | yes |
| <a name="input_kubernetes_auth_token"></a> [kubernetes\_auth\_token](#input\_kubernetes\_auth\_token) | Service account token | `string` | `""` | no |
| <a name="input_metallb_addresses"></a> [metallb\_addresses](#input\_metallb\_addresses) | IP addresses for metallb to serve | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->