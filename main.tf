module "metallb" {
  source            = "./modules/metallb"
  metallb_addresses = var.metallb_addresses
}