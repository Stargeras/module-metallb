terraform {
  required_providers {
    kubectl = {
      source = "gavinbunney/kubectl"
    }
  }
}

provider "helm" {
  kubernetes {
    host                   = var.kubernetes_auth_host
    cluster_ca_certificate = var.kubernetes_auth_cluster_ca_certificate
    client_certificate     = var.kubernetes_auth_client_certificate
    client_key             = var.kubernetes_auth_client_key
    token                  = var.kubernetes_auth_token
  }
}

provider "kubectl" {
  host                   = var.kubernetes_auth_host
  cluster_ca_certificate = var.kubernetes_auth_cluster_ca_certificate
  token                  = var.kubernetes_auth_token
  client_certificate     = var.kubernetes_auth_client_certificate
  client_key             = var.kubernetes_auth_client_key
  load_config_file       = false
}
/*
provider "kubernetes" {
  host                   = var.kubernetes_auth_host
  cluster_ca_certificate = var.kubernetes_auth_cluster_ca_certificate
  client_certificate     = var.kubernetes_auth_client_certificate
  client_key             = var.kubernetes_auth_client_key
  token                  = var.kubernetes_auth_token
}
*/