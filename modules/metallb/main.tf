resource "helm_release" "metallb" {
  name             = "metallb"
  namespace        = "metallb-system"
  create_namespace = "true"
  repository       = "https://charts.bitnami.com/bitnami"
  chart            = "metallb"
}

resource "kubectl_manifest" "ipaddresspool" {
  yaml_body = <<YAML
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  # A name for the address pool. Services can request allocation
  # from a specific address pool using this name.
  name: pool-01
  namespace: ${helm_release.metallb.namespace}
spec:
  # A list of IP address ranges over which MetalLB has
  # authority. You can list multiple ranges in a single pool, they
  # will all share the same settings. Each range can be either a
  # CIDR prefix, or an explicit start-end range of IPs.
  addresses:
  - ${var.metallb_addresses}
YAML
}

resource "kubectl_manifest" "l2advertisement" {
  yaml_body = <<YAML
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: advertise
  namespace: ${helm_release.metallb.namespace}
YAML
}
